#pragma once

void initReceive(char* storeChar);
BOOL receive(char* storeChar);
BOOL send(char* inputChar);
BOOL clearPortToBeClosed();
BOOL changeSomePortSettings(LPCSTR lpszCommName, DWORD BaudRate, DWORD Parity, BYTE ByteSize, DWORD StopBits);
HANDLE initPhysicalPort(LPCSTR lpszCommName);
BOOL setUpPhysicalSerialPort(DWORD BaudRate, DWORD Parity, BYTE ByteSize, DWORD StopBits);
BOOL SetCommunicationTimeouts(DWORD ReadIntervalTimeout, DWORD ReadTotalTimeoutMultiplier,
	DWORD ReadTotalTimeoutConstant, DWORD WriteTotalTimeoutMultiplier, DWORD WriteTotalTimeoutConstant);