#pragma once

HANDLE initPort(LPCSTR lpszCommName);
BOOL setUpSerialPort(DWORD BaudRate, DWORD Parity, BYTE ByteSize, DWORD StopBits);
BOOL changePortSettings(LPCSTR lpszCommName, DWORD BaudRate, DWORD Parity, BYTE ByteSize, DWORD StopBits);
void closeConnection();
void sendChar(char *);
void receiveChar(char* storeChar);
