#pragma once
const char EOT = 0x04;
const char ENQ = 0x05;
const char ACK = 0x06;
const char DC1 = 0x11;
const char DC2 = 0x12;
const char SYN0 = 0x0F;
const char SYN1 = 0xF0;
const int DATABUFFER = 512;
const int PACKETBUFFER = 516;