#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include "application.h"
#include "session.h"
#include "Resource.h"
#include <commdlg.h>

DWORD __stdcall receiveCharThread(LPVOID lpParam);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);
DWORD __stdcall monitorAndPrintReceive(LPVOID);


TCHAR Name[] = TEXT("Emulator");//main window name
char outputBuffer[10] = ""; //store output
char inputBuffer[10] = "";//stoer input 

HANDLE threadHandle;
DWORD threadID;
HWND hwnd;//main window handle
HWND openFileButton;
#pragma warning (disable: 4096)


// WinMain is the conventional name used for the application entry point.
// @param hInst A handle to the current instance of the application.
// @param lspszCmdParam The command line for the application, 
// @param nCmdShow Controls how the window is to be shown.
//  
// @return if the function succeeds, terminating when it receives a WM_QUIT message, return 0 if fail.
int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hprevInstance,
	LPSTR lspszCmdParam, int nCmdShow)
{

	MSG Msg;
	WNDCLASSEX Wcl;

	Wcl.cbSize = sizeof(WNDCLASSEX);
	Wcl.style = CS_HREDRAW | CS_VREDRAW;//set window style
	Wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION); // large icon 
	Wcl.hIconSm = NULL; // use small version of large icon
	Wcl.hCursor = LoadCursor(NULL, IDC_ARROW);  // cursor style

	Wcl.lpfnWndProc = WndProc; //set message processing function for main window
	Wcl.hInstance = hInst;//current instance of the application
	Wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); //white background
	Wcl.lpszClassName = Name;

	Wcl.lpszMenuName = MAKEINTRESOURCE(IDC_MY3980ASSIGNMENT1); // The menu Class
	Wcl.cbClsExtra = 0;      // no extra memory needed
	Wcl.cbWndExtra = 0;

	if (!RegisterClassEx(&Wcl))//register window
		return 0;

	hwnd = CreateWindow(Name, Name, WS_OVERLAPPEDWINDOW, 700, 350,
		800, 700, NULL, NULL, hInst, NULL);//create window
	CreateWindowW(L"STATIC", L"Statistics", WS_VISIBLE | WS_CHILD | WS_BORDER,
		580, 400, 150, 100, hwnd, NULL, NULL, NULL);
	openFileButton = CreateWindowW(L"BUTTON", L"Send File", WS_VISIBLE | WS_CHILD | WS_BORDER,
		580, 100, 150, 30, hwnd, (HMENU)2, NULL, NULL);
	DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1),
		hwnd, reinterpret_cast<DLGPROC>(DlgProc));//create and show command dialog before main window.

	ShowWindow(hwnd, nCmdShow);// if dialog end, show main window.
	UpdateWindow(hwnd);//constantly update main window


	while (GetMessage(&Msg, NULL, 0, 0))
	{
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}//message cycle

	return Msg.wParam;
}

int width = 0;//output cursor x position
int height = 0;//output cursor y position

// WndProc is the function processing message for main window.
// @ parm hwnd A handle to the window.
// @ parm Message The message.
// @ parm wParam Additional message information. The contents of this parameter depend on the value of the uMsg parameter.
// @ parm lParam Additional message information. The contents of this parameter depend on the value of the uMsg parameter.
// @ return the result of the message processing and depends on the message sent
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message,
	WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	char buffer;//buffer for key pressed

	switch (Message)
	{

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case 2:
			openDialog();
		case ID_CONNECTION_EXIT://if click Exit , exit the program.
			PostQuitMessage(0);
			break;
		case ID_CONNECTION_SETTING://if click setting, go command dialog.
			closeConnection();
			DialogBox(NULL, MAKEINTRESOURCE(IDD_DIALOG1), hwnd, reinterpret_cast<DLGPROC>(DlgProc));
			break;
		case ID_PORT_COM1://set up handler for menu item
			deActiveThread();
			changePortSettings((LPCSTR)L"COM1", NULL, NULL, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_PORT_COM2:
			deActiveThread();
			changePortSettings((LPCSTR)L"COM2", NULL, NULL, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_PORT_COM3:
			deActiveThread();
			changePortSettings((LPCSTR)L"COM3", NULL, NULL, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_PORT_COM4:
			deActiveThread();
			changePortSettings((LPCSTR)L"COM4", NULL, NULL, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_SPEED_2400:
			deActiveThread();
			changePortSettings(NULL, 2400, NULL, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_SPEED_9600:
			deActiveThread();
			changePortSettings(NULL, 9600, NULL, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_SPEED_32000:
			deActiveThread();
			changePortSettings(NULL, 38400, NULL, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_SPEED_110000:
			deActiveThread();
			changePortSettings(NULL,115200, NULL, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_PARITY_NONE:
			deActiveThread();
			changePortSettings(NULL, NULL, PARITY_NONE, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_PARITY_EVEN:
			deActiveThread();
			changePortSettings(NULL, NULL, PARITY_EVEN, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_PARITY_ODD:
			deActiveThread();
			changePortSettings(NULL, NULL, PARITY_ODD, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_PARITY_MARK:
			deActiveThread();
			changePortSettings(NULL, NULL, PARITY_MARK, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_BITSIZE_8:
			deActiveThread();
			changePortSettings(NULL, NULL, NULL, 8, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_BITSIZE_7:
			deActiveThread();
			changePortSettings(NULL, NULL, NULL, 7, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_BITSIZE_6:
			deActiveThread();
			changePortSettings(NULL, NULL, NULL, 6, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_BITSIZE_5:
			deActiveThread();
			changePortSettings(NULL, NULL, NULL, 5, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_PARITY_SPACE:
			deActiveThread();
			changePortSettings(NULL, NULL, PARITY_SPACE, NULL, NULL);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_STOPBIT_1:
			deActiveThread();
			changePortSettings(NULL, NULL, NULL, NULL, STOPBITS_10);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_STOPBIT_2:
			deActiveThread();
			changePortSettings(NULL, NULL, NULL, NULL, STOPBITS_15);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_STOPBIT_3:
			deActiveThread();
			changePortSettings(NULL, NULL, NULL, NULL, STOPBITS_20);//change Port settings, NULL here means keep original properties
			receiveChar(inputBuffer);//begin receiving
			threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
			break;
		case ID_HELP32781://if click help, then show some helping information.
			MessageBoxW(NULL, L"connected mode: Press ESC to go back to command mode. ", L"HELP", NULL);
			break;
		}
		
		break;


	case WM_CHAR:	// Process keystroke
		switch (wParam)
		{
		case VK_ESCAPE:
			closeConnection();//close handle when terminate connection.
			DialogBox(NULL, MAKEINTRESOURCE(IDD_DIALOG1), hwnd, reinterpret_cast<DLGPROC>(DlgProc));//pop up command dialog.
			break;
		default:
			hdc = GetDC(hwnd);			 // get device context
			buffer = wParam;//get which key pressed
			sendChar(&buffer);//send to session layer to process sending.
			sprintf_s(outputBuffer, "%c", LPCSTR(wParam)); // Convert char to string
			TextOutW(hdc, width, height, (LPCWSTR)outputBuffer, strlen(outputBuffer)); // output character
			ReleaseDC(hwnd, hdc); // Release device context
			width += 10;// move cursor to next character
			if (width == 580) {//change line
				width = 0;
				height += 20;
			}
			break;
		}
			
		break;

	case WM_DESTROY:	// Terminate program
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, Message, wParam, lParam);//default windows message processing function
	}
	return 0;
}



// This function process the message of command dialog.
// @ parm hWndDlg A handle to the window.
// @ parm Msg The message.
// @ parm wParam Additional message information. The contents of this parameter depend on the value of the uMsg parameter.
// @ parm lParam Additional message information. The contents of this parameter depend on the value of the uMsg parameter.
// @ return the result of the message processing and depends on the message sent

LRESULT CALLBACK DlgProc(HWND hWndDlg, UINT Msg, WPARAM wParam, LPARAM lParam)
{

	switch (Msg)
	{
	case WM_SYSCOMMAND:
		if (wParam == SC_CLOSE)
		{
			PostQuitMessage(0);//退出
		}
		return 0;
	case WM_INITDIALOG:


		//initialize baudrates
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"110");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"300");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"1200");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"2400");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"4800");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"9600");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"19200");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"38400");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"57600");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"115200");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"230400");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"460800");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"921600");
		//set default value for baudrates to 9600
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO1), CB_SETCURSEL, (WPARAM)5, 0);

		//initialize parity
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO2), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"None");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO2), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"Even");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO2), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"Odd");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO2), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"Mark");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO2), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"Space");
		//set default value for parity to none
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO2), CB_SETCURSEL, (WPARAM)0, 0);

		//initialize bit size
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO3), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"8");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO3), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"7");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO3), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"6");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO3), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"5");

		//set default value for baudrates to 8
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO3), CB_SETCURSEL, (WPARAM)0, 0);

		//initialize stop bit
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO4), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"1");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO4), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"1.5");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO4), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"2");
		//set default value for stop bit to 1
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO4), CB_SETCURSEL, (WPARAM)0, 0);

		//initialize port
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO5), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"COM1");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO5), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"COM2");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO5), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"COM3");
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO5), CB_ADDSTRING, (WPARAM)0, (LPARAM)L"COM4");
		//set default value for port to "COM3"
		SendMessageW(GetDlgItem(hWndDlg, IDC_COMBO5), CB_SETCURSEL, (WPARAM)2, 0);

		return TRUE;
	
	case WM_COMMAND:
		switch (wParam)
		{
		case IDC_BUTTON3:
			MessageBoxW(hWndDlg, L"connected mode: Press ESC to go back to command mode. ", L"HELP", MB_OK);
			break;
		case IDOK://click connect in comman dialog, this will initialize connection
			wchar_t baudRates[20];
			wchar_t parity[20];
			wchar_t bitSize[20];
			wchar_t stopBits[20];
			wchar_t port[20];

			//get port properties
			GetDlgItemTextW(hWndDlg, IDC_COMBO1, baudRates, sizeof baudRates);
			GetDlgItemTextW(hWndDlg, IDC_COMBO2, parity, sizeof parity);
			GetDlgItemTextW(hWndDlg, IDC_COMBO3, bitSize, sizeof bitSize);
			GetDlgItemTextW(hWndDlg, IDC_COMBO4, stopBits, sizeof stopBits);
			GetDlgItemTextW(hWndDlg, IDC_COMBO5, port, sizeof port);

			int parityBit, stopBit;

			// convert user input to correct parity setting value
			if (parity[0] == 'E')
				parityBit = EVENPARITY;
			if (parity[0] == 'O')
				parityBit = ODDPARITY;
			if (parity[0] == 'N')
				parityBit = NOPARITY;
			if (parity[0] == 'M')
				parityBit = MARKPARITY;
			if (parity[0] == 'S')
				parityBit = SPACEPARITY;

			// convert user input to correct stop bit setting value
			if (_wtoi(stopBits) == 1)
				stopBit = ONESTOPBIT;
			if (_wtoi(stopBits) == 1.5)
				stopBit = ONE5STOPBITS;
			if (_wtoi(stopBits) == 2)
				stopBit = TWOSTOPBITS;

			//set up port properties and initialize port
			if (initPort((LPCSTR)port) && setUpSerialPort(_wtoi(baudRates), parityBit,
				_wtoi(bitSize), ONESTOPBIT)) {
				setUpSerialPort(_wtoi(baudRates), parityBit, _wtoi(bitSize), ONESTOPBIT);//set up port properties
				receiveChar(inputBuffer);//begin receiving
				threadHandle = CreateThread(NULL, 0, monitorAndPrintReceive, nullptr, 0, &threadID);//create thread to print str constantly
				EndDialog(hWndDlg, 0); //after connection, end command dialog.
			}
			//FOR TESTING!!!!!!!
			EndDialog(hWndDlg, 0); //after connection, end command dialog.
			break;

		case IDC_BUTTON1://press exit, exit the program
			PostQuitMessage(0);
			break;
		}

		break;


		
	}

	return FALSE;
}
BOOL isActiveThread=0;//flag to set status of monitorAndPrintReceive thread.
int widthReceive = 0;
int heightReceive = 200;
//This function keep printing string received.
// @param lpParam pointer for passing data
// @return default return 0
DWORD __stdcall monitorAndPrintReceive(LPVOID lpParam)
{

	isActiveThread = 1;
	while (isActiveThread) {
		if (inputBuffer[0] != NULL) {
			HDC hdc = GetDC(hwnd);// get device context
			TextOutA(hdc, widthReceive, heightReceive, inputBuffer, strlen(inputBuffer));//print received text to screen. 
			ReleaseDC(hwnd, hdc); // Release device context
			widthReceive += 10; // move cursor to next postion

			//switch line
			if (widthReceive == 580) {
				widthReceive = 0;
				heightReceive += 20;
			}
			inputBuffer[0] = NULL; //reset receieving buffer
		}

	}

	return 0;
}
//set thread to be on state.
void deActiveThread() {
	isActiveThread = 0;
}

void openDialog() {
	OPENFILENAME ofn;       // common dialog box structure
	char szFile[260];       // buffer for file name
	HWND hwnd = { 0 };      // owner window
	HANDLE hf;              // file handle
	char buffer[1024];

	LPDWORD numByteRead = 0;

// Initialize OPENFILENAME
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
// Set lpstrFile[0] to '\0' so that GetOpenFileName does not 
// use the contents of szFile to initialize itself.
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = "Text\0*.TXT\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

// Display the Open dialog box. 
if (GetOpenFileName(&ofn) == TRUE) {
	hf = CreateFile(ofn.lpstrFile, GENERIC_READ | GENERIC_WRITE, 0,
		(LPSECURITY_ATTRIBUTES)NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, (HANDLE)NULL);
	int filesize = GetFileSize(hf, NULL);
	int tmp;
	OutputDebugString(szFile);
	//while (filesize > 0) {
	//	/*if ((tmp = filesize % 516) != 0) {
	//	ReadFile(hf, buffer, tmp, numByteRead, NULL);
	//	filesize -= tmp;
	//	}
	//	else {
	//	ReadFile(hf, buffer, 516, numByteRead, NULL);
	//	filesize -= 516;
	//	}*/
	//	if (filesize >= 512) {
	//		ReadFile(hf, buffer, 512, numByteRead, NULL);
	//		filesize -= 512;
	//	}
	//	else {
	//		ReadFile(hf, buffer, filesize, numByteRead, NULL);
	//		buffer[filesize] = '\0';
	//		filesize = 0;
	//		//wantToSend = true;
	//	}
	//	//output += buffer;
	//	OutputDebugString(buffer);
	//	//paint(buffer);
	//}
	////paint(output);
	CloseHandle(hf);
}
}