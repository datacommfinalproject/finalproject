#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include "physical.h"
#include "session.h"

HANDLE hComm;
BOOL activeThread=0;

//This function creates serial port connnection.
//@param lpszCommName char pointer to port name
//@return Handle of the port, 0 if fail to connect.
HANDLE initPhysicalPort(LPCSTR lpszCommName) {

	//change FILE_FLAG_OVERLAPPED to 0, we donot need overlap mode for now.
	if ((hComm = CreateFile(lpszCommName, GENERIC_READ | GENERIC_WRITE, 0,
		NULL, OPEN_EXISTING, 0, NULL))
		== INVALID_HANDLE_VALUE)
	{
		MessageBox(NULL, TEXT("Error opening COM port:"), TEXT("ERROR"), MB_OK| MB_TOPMOST);
		return FALSE;
	}

	return hComm;

}
// This function set up key properties of serial port.
//@param BaudRate is BaudRate passed by user.
//@param Parity is Parity passed by user.
//@param ByteSize is ByteSize passed by user.
//@param StopBits is StopBits passed by user
//@return true if success setting up the port, false othervise.
BOOL setUpPhysicalSerialPort(DWORD BaudRate, DWORD Parity, BYTE ByteSize, DWORD StopBits) {
	DCB properties;
	GetCommState(hComm, &properties);

	properties.BaudRate = BaudRate;
	properties.Parity = Parity;
	properties.ByteSize = ByteSize;
	properties.StopBits = StopBits;
	if (!SetCommState(hComm, &properties)) {
		MessageBox(NULL, TEXT("port setup failed!"), TEXT("ERROR"), MB_OK | MB_TOPMOST);
		return false;
	}
	return SetCommunicationTimeouts(0, 0, 10, 0, 0);//set up timeout
}
// This function set up timeouts for transimission.
BOOL SetCommunicationTimeouts(DWORD ReadIntervalTimeout,
	DWORD ReadTotalTimeoutMultiplier,
	DWORD ReadTotalTimeoutConstant,
	DWORD WriteTotalTimeoutMultiplier,
	DWORD WriteTotalTimeoutConstant)
{
	BOOL m_bPortReady;
	_COMMTIMEOUTS m_CommTimeouts;
	if ((m_bPortReady = GetCommTimeouts(hComm, &m_CommTimeouts)) == 0)
		return false;
	m_CommTimeouts.ReadIntervalTimeout = ReadIntervalTimeout;
	m_CommTimeouts.ReadTotalTimeoutConstant = ReadTotalTimeoutConstant;
	m_CommTimeouts.ReadTotalTimeoutMultiplier = ReadTotalTimeoutMultiplier;
	m_CommTimeouts.WriteTotalTimeoutConstant = WriteTotalTimeoutConstant;
	m_CommTimeouts.WriteTotalTimeoutMultiplier = WriteTotalTimeoutMultiplier;
	m_bPortReady = SetCommTimeouts(hComm, &m_CommTimeouts);

	if (m_bPortReady == 0)
	{
		MessageBox(NULL, TEXT("StCommTimeouts function failed Com Port Error"), TEXT("ERROR"), MB_OK | MB_TOPMOST);
		CloseHandle(hComm);
		return false;
	}
	return true;
}
//This function send method send one char to other terminal
// @param inputChar 
// @return true if send success, false othervise.
BOOL send(char* inputChar) {
	char* aChar = inputChar;
	BOOL isSent;
	unsigned long bytes_sent = 0;
	isSent = WriteFile(hComm, aChar, 1, &bytes_sent, NULL);//send aChar to other terminal.
	if (!isSent) {
		OutputDebugStringA("send fail\n");
	}

	return isSent;
}

//This function receive one char from other terminal. 
//@param storeChar is buffer used to store receive char.
//@return ture if receive success, false othervise.
BOOL receive(char* storeChar) {
	char buffer[10] = "";
	BOOL isReceived;
	unsigned long bytes_received = 0;
	if (isReceived = ReadFile(hComm, buffer, 1, &bytes_received, NULL)) {//receiving from other terminal.
		if (bytes_received > 0)
		strcpy_s(storeChar, 10, buffer);//copy inside buffer to buffer passed by other function 
		buffer[0] = NULL;//reset buffer
	}else
		OutputDebugStringA("receive fail.......\n");
	return isReceived;

}

//This function receiveCharThread receive constantly from other terminal, and 
//     are used to create thread.
//@param lpParam is void pointer used to pass value from thread creator.
//@return 0 if receive fail.
DWORD __stdcall receiveCharThread(LPVOID lpParam)
{
	char* storeChar = (char *)lpParam;
	while (receive((char*)lpParam)&& activeThread) {//create a receive cycle.
	}

	return 0;
}

//This function initReceive creates a receive thread.
//@param storeChar is buffer used for store received char.
void initReceive(char* storeChar) {
	HANDLE Thread;
	DWORD threadID;
	activeThread = 1;
	Thread = CreateThread(NULL, 0, receiveCharThread, storeChar, 0, &threadID);//create receive thread.
}

//This function exit the thread safely, and end communication.
//@return true if exit thread success, false othervise
BOOL clearPortToBeClosed() {
	activeThread = 0;//deactive thread safely
	return CloseHandle(hComm);
}

BOOL changeSomePortSettings(LPCSTR lpszCommName,DWORD BaudRate, DWORD Parity, BYTE ByteSize, DWORD StopBits) {

	DCB dcb;
	if (!GetCommState(hComm, &dcb)) 
		return false;

	LPCSTR lpszCommNameTemp = (LPCSTR)L"COM3";
	DWORD BaudRateTemp = dcb.BaudRate;
	DWORD ParityTemp =dcb.Parity;
	BYTE ByteSizeTemp = dcb.ByteSize;
	DWORD StopBitsTemp =dcb.StopBits;

	clearPortToBeClosed();
	if (lpszCommName != NULL)
		lpszCommNameTemp = lpszCommName;
	if (BaudRate != NULL)
		BaudRateTemp = BaudRate;
	if (Parity != NULL)
		ParityTemp = Parity;
	if (ByteSize != NULL)
		ByteSizeTemp = ByteSize;
	if (StopBits != NULL)
		StopBitsTemp = StopBits;


	if (initPhysicalPort((LPCSTR)lpszCommNameTemp) && setUpPhysicalSerialPort(BaudRateTemp, ParityTemp, ByteSizeTemp, StopBitsTemp))
		return true;
	return false;
	
}