//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 3980_Assignment1.rc
//
#define IDC_MYICON                      2
#define IDD_MY3980ASSIGNMENT1_DIALOG    102
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDC_MY3980ASSIGNMENT1           109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDC_COMBO1                      1000
#define IDC_COMBO2                      1001
#define IDC_COMBO3                      1002
#define IDC_COMBO4                      1003
#define IDC_COMBO5                      1004
#define IDC_BUTTON2                     1010
#define IDC_BUTTON1                     1010
#define IDC_BUTTON3                     1011
#define ID_HELP                         32771
#define ID_EXIT                         32772
#define ID_EXIT_3                       32773
#define s                               32774
#define ID_CONNECTION_PROT              32775
#define ID_HELP_H                       32776
#define ID_CONNECTION_PORT              32777
#define ID_CONNECTION_BAUDRATES         32778
#define ID_CONNECTION_SETTING           32779
#define ID_CONNECTION_EXIT              32780
#define ID_HELP32781                    32781
#define ID_CONNECTION_PORT32782         32782
#define ID_CONNECTION_SPEED             32783
#define ID_PORT_COM1                    32784
#define ID_PORT_COM2                    32785
#define ID_PORT_COM3                    32786
#define ID_PORT_COM4                    32787
#define ID_SPEED_9600                   32788
#define ID_SPEED_32000                  32789
#define ID_SPEED_110000                 32790
#define ID_SPEED_2400                   32791
#define ID_CONNECTION_WORDSIZE          32792
#define ID_CONNECTION_BITSIZE           32793
#define ID_BITSIZE_8                    32794
#define ID_BITSIZE_7                    32795
#define ID_BITSIZE_6                    32796
#define ID_BITSIZE_5                    32797
#define ID_CONNECTION_PARITY            32798
#define ID_PARITY_NONE                  32799
#define ID_PARITY_EVEN                  32800
#define ID_PARITY_ODD                   32801
#define ID_PARITY_MARK                  32802
#define ID_PARITY_SPACE                 32803
#define ID_CONNECTION_STOPBIT           32804
#define ID_STOPBIT_1                    32805
#define ID_STOPBIT_2                    32806
#define ID_STOPBIT_3                    32807
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32808
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
