#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include "session.h"
#include "physical.h"
#include "application.h"

//This function creates serial port connnection.
//@param lpszCommName char pointer to port name
//@return Handle of the port, 0 if fail to connect.
HANDLE initPort(LPCSTR lpszCommName) {
	return initPhysicalPort(lpszCommName);
}

// This function set up key properties of serial port.
//@param BaudRate is BaudRate passed by user.
//@param Parity is Parity passed by user.
//@param ByteSize is ByteSize passed by user.
//@param StopBits is StopBits passed by user
//@return true if success setting up the port, false othervise.
BOOL setUpSerialPort(DWORD BaudRate, DWORD Parity, BYTE ByteSize, DWORD StopBits) {
	SetCommunicationTimeouts(0, 0, 10, 0, 0);
	return setUpPhysicalSerialPort(BaudRate, Parity, ByteSize, StopBits);
}

// This function send buffer to physical layer from application layer
// @param userInput accept keyboard input from application layer.
void sendChar(char* userInput) {
	send(userInput);
}
// This function initilize receive cycle of serial port.
//@param storeChar is char buffer receieved from application layer, going to physical layer. 
void receiveChar(char* storeChar)
{
	initReceive(storeChar);
}
//close port connection.
void closeConnection() {
	clearPortToBeClosed();
}

//change port setting, can keep unchanged properties.
//@param BaudRate is BaudRate passed by user,NULL if no change.
//@param Parity is Parity passed by user,NULL if no change.
//@param ByteSize is ByteSize passed by user,NULL if no change.
//@param StopBits is StopBits passed by user,NULL if no change.
//@param lpszCommName char pointer to port name, NULL if no change
//@return true if changing been made, false if fail.
BOOL changePortSettings(LPCSTR lpszCommName, DWORD BaudRate, DWORD Parity, BYTE ByteSize, DWORD StopBits) {
	return changeSomePortSettings(lpszCommName, BaudRate, Parity, ByteSize, StopBits);
}


